package INF102.lab5.graph;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
        Map<V, Boolean> found = new HashMap<>();
        for (V node : graph.getNodes()) {
            found.put(node, false);
        }

        return depthFirst(u, v, found);
    }

    public boolean depthFirst(V u, V v, Map<V, Boolean> found) {
        found.put(u, true);
        for (V next : graph.getNeighbourhood(u)) {
            if (next.equals(v)) {
                return true;
            }
            if (!found.get(next)) {
                if (depthFirst(next, v, found)) {
                    return true;
                }
            }
        }
        return false;
    }
}
